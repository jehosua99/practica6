#include "ball.h"
#include <time.h>
#include <math.h>

Ball::Ball()
{
    srand (time(NULL));
    PosX = rand() % 801 + 50;
    PosY = rand() % 301 + 50;       //Defino los valores aleatorios para las bolas
    Rad = rand() % 41 + 10;
    VelX = rand() % 151 + 150;
    VelY = rand() % 201 + 100;
    Mass = rand() % 51 + 550;
    Rest = rand() % 6 + 5;
    Rest /= 10;           //Lo divido entre 10 para tener valores entre 0.5 y 1
    Fric = rand() % 11;
    Fric /= 1000;
    densidad = 1.01;
    pi = 3.1415926;
    constante = pi*densidad/2;
    g = 10;
}

QRectF Ball::boundingRect() const
{
    return QRectF(PosX, PosY, 2*Rad, 2*Rad);
}

void Ball::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPixmap pixmap;
    pixmap.load(":/580b585b2edbce24c47b270b.png");  //Creo las bolas con una imagen del archivo
    painter->drawPixmap(boundingRect(),pixmap,pixmap.rect());
}

void Ball::mover()
{
    float vel, theta, acelX, acelY, t = 0.01, velx, vely;
    vel = (pow(VelX, 2) + pow(VelY, 2));
    theta = atanf(VelY/VelX);
    acelX = -((constante*VelX*pow(Rad, 2)*cos(theta)*Fric)/Mass);
    acelY = -((constante*VelY*pow(Rad, 2)*sin(theta)*Fric)/Mass)-g;
    velx = VelX + (acelX*t);
    vely = VelY + (acelY*t);
    PosX = PosX + (VelX*t) + (acelX*pow(t, 2));
    PosY = PosY + (VelY*t) + (acelY*pow(t, 2));
    VelX = velx;
    VelY = vely;
    setPos(x()+PosX, y()+PosY);
}

void Ball::choqueY()  //Será llamado cuando identifique un choque con los bordes verticales
{
    float theta, acelX, acelY, t = 0.01, velx, vely;
    VelX = -(Rest*VelX);
    theta = atanf(VelY/VelX);
    acelX = -((constante*VelX*pow(Rad, 2)*cos(theta)*Fric)/Mass);
    acelY = -((constante*VelY*pow(Rad, 2)*sin(theta)*Fric)/Mass)-g;
    vely = VelY + (acelY*t);
    PosX = PosX + (VelX*t) + (acelX*pow(t, 2));
    PosY = PosY + (VelY*t) + (acelY*pow(t, 2));
    VelY = vely;
    setPos(x()+PosX, y()+PosY);
}

void Ball::choqueX() //Será llamado cuando identifique un choque con los bordes horizontales
{
    float theta, acelX, acelY, t = 0.1, velx, vely;
    VelY = -(Rest*VelY);
    theta = atanf(VelY/VelX);
    acelX = -((constante*VelX*pow(Rad, 2)*cos(theta)*Fric)/Mass);
    acelY = -((constante*VelY*pow(Rad, 2)*sin(theta)*Fric)/Mass)-g;
    velx = VelX + (acelX*t);
    PosX = PosX + (VelX*t) + (acelX*pow(t, 2));
    PosY = PosY + (VelY*t) + (acelY*pow(t, 2));
    VelX = velx;
    setPos(x()+PosX, y()+PosY);
}
