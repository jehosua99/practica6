#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ball.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene=new QGraphicsScene(0,0,1000,500); //Defino el tamaño de la escena
    ui->graphicsView->setScene(scene);
    ui->graphicsView->setBackgroundBrush(QImage(":/44a65d26e19c91ad534cfa13fe60c32ba398946da1b81530712abc29504ba8bc.jpg"));  //Agrego un fondo a la escena
    crear = false;  // Bandera que me indicara cuando crear una nueva bola, esta en false por defecto porque no se desea crear nuevas bolas
    timer=new QTimer();
    connect(timer,SIGNAL(timeout()),this,SLOT(animar()));
    timer->start(100);

    bolas.append(new Ball());  //Creo una nueva bola al iniciar el programa por defecto
    scene->addItem(bolas.last());
    bolas.last();

    HorSup=new QGraphicsLineItem(10, 10, 980, 10);  //Creacion de los bordes con los cuales chocara la bola
    VerLef=new QGraphicsLineItem(10, 10, 10, 480);
    VerRig=new QGraphicsLineItem(980, 10, 980, 480);
    HorInf=new QGraphicsLineItem(10, 480, 980, 480);
    scene->addItem(HorSup);
    scene->addItem(VerLef);
    scene->addItem(HorInf);  //Muestro en escena los bordes
    scene->addItem(VerRig);
}

MainWindow::~MainWindow()
{
    delete scene;
    delete ui;
}
void MainWindow::animar()
{
    if (crear == true)  //Si se detecta que la bandera ha cambiado a true se creara una nueva bola con valores aleatorios
    {
        bolas.append(new Ball());
        scene->addItem(bolas.last());
        bolas.last();
    }
    for (int i=0; i<bolas.length();i++)  //Aqui se verificara si la bola choca con algun borde
    {
        if (bolas.at(i)->collidesWithItem(HorInf) || bolas.at(i)->collidesWithItem(HorSup))
        {
            bolas.at(i)->choqueX();
        }
        if (bolas.at(i)->collidesWithItem(VerLef) || bolas.at(i)->collidesWithItem(VerRig))
        {
            bolas.at(i)->choqueY();
        }
        bolas.at(i)->mover();
        bolas.at(i)->setPos(x(), y());
    }
}

void MainWindow::keyPressEvent(QKeyEvent *ev)
{
    if(ev->key()==Qt::Key_N)  //Si detecta que ha sido presionada la tecla N cambiara la bandera a true
    {
        crear = true;
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *ev)
{
    if(ev->key()==Qt::Key_N)  //Si la tecla es soltada cambiara la bandera a false
    {
        crear = false;
    }
}
