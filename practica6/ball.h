#ifndef BALL_H
#define BALL_H

#include <QGraphicsItem>
#include <QPainter>

class Ball: public QGraphicsItem
{
public:
    Ball();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mover();
    void choqueX();
    void choqueY();
    double PosX;
    double PosY;
private:
    float Rad;
    float Mass;
    double VelX;
    double VelY;
    float Rest;
    float Fric;
    int g;
    float constante;
    float densidad;
    float pi;
};

#endif // BALL_H
